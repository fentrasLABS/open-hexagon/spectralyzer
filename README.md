# Spectralyzer
Analyze audio files and extract useful beat data for [Open Hexagon](https://openhexagon.org) levels.

## Quick Start
- You should install these requirements first:
  - `matplotlib`
  - `argparse`
  - `librosa`
  - `numpy`
- Tested on *Python 3.9.7*.
- Place `Spectralyzer.py` into a directory with your `.lua` level.
- Assuming this pack structure:
  - *Script* `Scripts/MyLevel/Spectralyzer.py`.
  - *Level*: `Scripts/MyLevel/level.lua`.
  - *Music*: `Music/music.ogg`.
- Run `python Spectralyzer.py ../../Music/music.ogg --strength -p`.
- This will generate basic *onset* events and show the results.
  - Useful for rapid changes (but can have zero effect depending on the amplitude).
- You can experiment with the data by providing additional configuration arguments.
- Try using different method for data generation, for example `--aggregate mean`.
  - This will return an *average* instead of a *median*.    
- Or generate *root-mean-square* events using `--power`.
  - Useful for continuous changes (but can pickup rapid changes as well).
- All generated files are placed into `audio/` folder by default.
- Use `--help` to output all available commands.
- Enjoy!

## Wiki
For a full guide refer to this repository [Wiki](https://gitlab.com/fentrasLABS/open-hexagon/spectralyzer/wikis/home).

![](_media/cover.webp)