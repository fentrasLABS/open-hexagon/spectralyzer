import matplotlib.pyplot as plt
import librosa.display as lrd
import os.path as path
import argparse as ap
import librosa as lr
import numpy as np
import sys, os

OUTPUT_FOLDER   = 'audio'
OUTPUT_FILE     = 'lua'

STRENGTH_FILE   = 'strength'
POWER_FILE      = 'power'
PEAK_FILE       = 'peak'

STRENGTH_AGGR   = 'median'
STRENGTH_MAX    = (3, 3)
STRENGTH_AVG    = (3, 5)
STRENGTH_DELTA  = 0.5
STRENGTH_WAIT   = 10

COLORMAP_TYPE   = 'Blues'

pr = ap.ArgumentParser(description='Analyze audio files and extract useful beat data for Open Hexagon levels.')

pr.add_argument('path', type=str, help='path to audio file')

pr_evnt = pr.add_argument_group('events arguments')
# https://librosa.org/doc/latest/generated/librosa.onset.onset_strength.html
pr_evnt.add_argument('--strength', action='store_true', help='create onset events')
# https://librosa.org/doc/latest/generated/librosa.feature.rms.html
pr_evnt.add_argument('--power', action='store_true', help='create root-mean-square events')
# https://librosa.org/doc/latest/generated/librosa.util.peak_pick.html
pr_evnt.add_argument('--peak', action='store_true', help='create peak-only events')

pr_conf = pr.add_argument_group('configuration arguments')
# https://numpy.org/doc/stable/reference/generated/numpy.median.html
pr_conf.add_argument('--aggregate', metavar='METHOD', type=str, action='store', default=STRENGTH_AGGR, help='use different aggregation method for strength and peak')
# https://librosa.org/doc/latest/generated/librosa.util.peak_pick.html
pr_conf.add_argument('--max', metavar=('PRE', 'POST'), type=int, action='store', nargs=2, default=STRENGTH_MAX, help='number of samples over which max is computed for peak')
pr_conf.add_argument('--avg', metavar=('PRE', 'POST'), type=int, action='store', nargs=2, default=STRENGTH_AVG, help='number of samples over which mean is computed for peak')
pr_conf.add_argument('--delta', metavar=('OFFSET'), type=float, action='store', default=STRENGTH_DELTA, help='mean threshold offset for peak')
pr_conf.add_argument('--wait', metavar=('SAMPLES'), type=int, action='store', default=STRENGTH_WAIT, help='number of samples to wait after picking a peak')

pr_plot = pr.add_argument_group('graphing arguments')
pr_plot.add_argument('-p', '--plot', action='store_true', help='draw spectogram with events')
# https://matplotlib.org/stable/tutorials/colors/colormaps.html
pr_plot.add_argument('-m', '--colormap', metavar='C', type=str, action='store', default=COLORMAP_TYPE, help='use different colormap for the spectrogram')

pr_optm = pr.add_argument_group('optimization arguments')
# https://numpy.org/doc/stable/reference/generated/numpy.clip.html
pr_optm.add_argument('-c', '--clip', metavar=('A', 'B'), type=float, action='store', nargs=2, help='clip output between minimum and maximum values')
# https://numpy.org/doc/stable/reference/generated/numpy.interp.html
pr_optm.add_argument('-l', '--lerp', metavar=('A', 'B'), type=float, action='store', nargs=2, help='convert initial range to a custom range')

pr_file = pr.add_argument_group('output arguments')
pr_file.add_argument('-o', '--output', metavar='D', type=str, action='store', default=OUTPUT_FOLDER, help='destination for generated files')
pr_file.add_argument('-e', '--extension', metavar='E', type=str, action='store', default=OUTPUT_FILE, help='extension for generated files')
pr_file.add_argument('-b', '--block', action='store_true', help='prevent file(s) generation')
pr_file.add_argument('-v', '--verbose', action='store_true', help='show full output in the console')

args = pr.parse_args()

sum_events = lambda: int(args.strength or args.peak) + int(args.power)

if (sum_events() == 0):
    args.plot = True
    if (args.clip or args.lerp):
        sys.exit("at least one event is required for optimization arguments")

window_header = path.basename(args.path)

y, sr = lr.load(args.path)

stft = np.abs(lr.stft(y=y))
times = lr.times_like(stft)

def optimize(data):
    if (args.clip):
        data = np.clip(data, args.clip[0], args.clip[1])
    if (args.lerp):
        data = np.interp(data, (data.min(), data.max()), (args.lerp[0], args.lerp[1]))
    return data

if args.strength or args.peak:
    cqt = np.abs(lr.cqt(y=y, sr=sr))
    db = lr.amplitude_to_db(cqt, ref=np.max)
    onset = lr.onset.onset_strength(sr=sr, S=db, aggregate=getattr(np, args.aggregate))
    if args.peak:
        peaks = lr.util.peak_pick(onset, pre_max=args.max[0], post_max=args.max[1], pre_avg=args.avg[0], post_avg=args.avg[1], delta=args.delta, wait=args.wait)
    onset = optimize(onset)

if args.power:
    rms = lr.feature.rms(S=lr.amplitude_to_db(stft, ref=np.min))[0]
    rms = optimize(rms)

if args.plot:
    e = sum_events()
    e_i = 0

    fig, ax = plt.subplots(nrows=(e + 1), sharex=True, num=window_header)

    pick_ax = lambda i, n=0: ax[n] if i > 0 else ax
    
    lrd.specshow(lr.amplitude_to_db(stft, ref=np.max), y_axis='log', x_axis='time', ax=pick_ax(e), cmap=args.colormap)

    pick_ax(e).set(title=window_header)
    pick_ax(e).label_outer()

    if (args.strength or args.peak):
        e_i += 1
        ax[e_i].plot(times, onset)
        ax[e_i].set(ylabel='Strength')
        if (args.peak):
            ax[e_i].vlines(times[peaks], 0, onset.max(), color='r', alpha=0.75, label='Peaks')
            ax[e_i].legend(loc='upper right')
    
    if (args.power):
        e_i += 1
        ax[e_i].plot(times, rms)
        ax[e_i].set(ylabel='Power')

    pick_ax(e, e_i).set(xlabel='Time')

if sum_events() > 0 and not args.block or args.verbose:
    def cprint(text='', file=None, remove_n=False):
        if (file and not args.block):
            file.write(text)
        if (not file or args.verbose):
            if (remove_n):
                text = text.replace(chr(10), '')
            print(text)

    def output_header(constant, values, iterable):
        values = [values[i] for i in iterable]
        min = f"{constant.upper()}_MIN = {np.min(values):.3f}"
        max = f"{constant.upper()}_MAX = {np.max(values):.3f}"
        start = f"{constant.upper()} = {{"
        return f"{min}\n{max}\n{start}\n"

    def output_data(values, iterable, item, index):
        time = f"{times[item]:.3f}"
        value = f"{values[item]:.3f}"
        if index < len(iterable) - 1:
            separator = ',\n'
        else:
            separator = '\n}'
        return f"\t{{ {time}, {value} }}{separator}"

    def iterate_over(iterable, data, type):
        if (not args.block):
            entry = globals()[f"{type.upper()}_FILE"] + '.' + args.extension
            file = open(path.join(args.output, entry), 'w')
        cprint(output_header(type, data, iterable), file)
        for index, item in enumerate(iterable):
            cprint(output_data(data, iterable, item, index), file, True)
        if file:
            file.close()

    len_t = len(times)
    rng_t = range(len_t)

    if not args.block and not path.exists(args.output):
        os.makedirs(args.output)

    if args.strength:
        iterate_over(rng_t, onset, 'strength')
    if args.power:
        iterate_over(rng_t, rms, 'power')
    if args.peak:
        iterate_over(peaks, onset, 'peak')

if (args.plot):
    plt.show()